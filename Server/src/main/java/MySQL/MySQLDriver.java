package MySQL;

import Validator.Validation;

import java.sql.*;


public class MySQLDriver {
    private boolean success = false;
    private Validation validation = new Validation();
    private Connection connection;

    public MySQLDriver(){}

    public void startConnection(){
        final String url = "jdbc:mysql://duemmer.informatik.uni-oldenburg.de:48121/zugumzug";
//                +"?serverTimezone=Europe/Berlin";       //Fix für MySQL Connector ab Version 6 für den Fehler undefined Timezone
        final String user = "server";
        final String pass = "sagichnicht";
        try{
            connection = DriverManager.getConnection(url,user,pass);
            System.out.println("Verbindung zur Datenbank erfolgreich hergestellt.");

        } catch (Exception e){
            System.out.println("Verbindung zum Events.CommandListener ist derzeit nicht möglich.");
            System.out.println(e.getMessage());
        }
    }

    //Verbindung zur Datenbank stoppen
    public void stopConnection() throws SQLException {
        this.connection.close();
    }


    public boolean registerMe(String name, String password, String email) throws SQLException {
        success = false;

        if( !validation.validateName(name) && !validation.validatePassword(password) && !validation.validateEmail(email) ) {
            return isSuccessful();
        }

        name = validation.properCaseName(name);

        if(loginDoesntExist(name)) {
            String sql ="INSERT INTO user(user_name, user_password, email) VALUES (?,?,?);";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1,name);
            System.out.println(name);

            Crypto hashedPassword = new Crypto(password);
            password = hashedPassword.getSHA();

            statement.setString(2,password);
            System.out.println(password);
            statement.setString(3,email);
            System.out.println(email);

//            if ((statement.executeUpdate()) == 1)success = true;
            statement.execute();
            success = true;
        }
        return isSuccessful();
    }

    private boolean loginDoesntExist(String name) throws SQLException{
        boolean doesNotExist = true;
        String sql = "SELECT DISTINCT user_name FROM user WHERE user_name = ?;";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, name);

        ResultSet rs = statement.executeQuery();
        if(rs.next()){
           doesNotExist=false;
        }
        return doesNotExist;
    }


    public boolean logMeIn(String name, String password) throws SQLException {
        success = false;

        if( !validation.validateName(name) || !validation.validatePassword(password)) {
            return isSuccessful();
        }

        name = validation.properCaseName(name);

        String sql = "SELECT DISTINCT user_name FROM user WHERE user_name = ? AND user_password = ?;";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, name);


        Crypto hashedPassword = new Crypto(password);
        password = hashedPassword.getSHA();
        statement.setString(2, password);

        ResultSet resultset = statement.executeQuery();
        if (resultset.next()) {
            success = true;
        }
        resultset.close();
        return isSuccessful();
    }

    private boolean isSuccessful() {
        return success;
    }
}
