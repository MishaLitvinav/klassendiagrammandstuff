package MySQL;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Crypto {
    private String hashedPass;
    protected Crypto(String pass){
        try {
            MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
            //Salt für das Passwort
            pass = "@L18" + pass;

            sha256.update(pass.getBytes(StandardCharsets.UTF_8));
            byte[] digest = sha256.digest();

            hashedPass = String.format("%064x", new BigInteger(1,digest));
        } catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }


    }
    public String getSHA() {
        return hashedPass;
    }
}
