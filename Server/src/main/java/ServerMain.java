import Network.Server;

public class ServerMain {
    public static void main(String[] args)throws Exception{
        ConfigHandler cfgHand = new ConfigHandler();
        int port = 48120; // 48120
        System.out.println("Starting ServerPackage.ServerPackage on port " + port);
        Server server = new Server(port);
//        new ServerPackage.ServerPackage(new Events.CommandListener(), port).start();
        server.start();
    }
}
