package GameManagement.UserManagement;

import GameManagement.UserManagement.IUserManagement;
import GameManagement.UserManagement.User.IUserImpl;
import MySQL.MySQLDriver;

import java.sql.SQLException;
import java.util.ArrayList;

public class IUserManagentImpl implements IUserManagement {
    private MySQLDriver driver;
    private ArrayList<IUserImpl> loggedInUsers;

    public IUserManagentImpl(){
        loggedInUsers = new ArrayList<>();
        this.driver = new MySQLDriver();
        driver.startConnection();
    }

    @Override
    public boolean registerMe(String name, String password, String email) throws SQLException {
        boolean b = false;
        b = driver.registerMe(name, password, email);
        return b;
    }

    @Override
    public boolean logMeIn(String name, String password) throws SQLException {
        boolean b = false;
        b = driver.logMeIn(name, password);
        return b;
    }

    public ArrayList<IUserImpl> getLoggedInUsers(){
        return this.loggedInUsers;
    }

    public void addUser(IUserImpl user){
        loggedInUsers.add(user);
    }
}
