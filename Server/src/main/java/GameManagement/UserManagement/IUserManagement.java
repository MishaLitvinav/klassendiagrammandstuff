package GameManagement.UserManagement;

import java.sql.SQLException;

public interface IUserManagement {
    boolean registerMe(String name, String password, String email) throws SQLException;
    boolean logMeIn(String name, String passwort) throws SQLException;
    //int getTotalPointsEver(String username);
    //int getTotalGamesPlayed(String username);
}