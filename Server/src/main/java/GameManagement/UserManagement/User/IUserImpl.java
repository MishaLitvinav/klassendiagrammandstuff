package GameManagement.UserManagement.User;

import io.netty.channel.ChannelHandlerContext;

public class IUserImpl implements IUser{
    ChannelHandlerContext ctx;
    private String username;
    private String password;
    //private int level;


    public IUserImpl(String username, String password){
        this.username = username;
        this.password = password;
    }

    @Override
    public String getUserName() {
        return this.username;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public void setUserName(String username){
        this.username = username;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }
}
