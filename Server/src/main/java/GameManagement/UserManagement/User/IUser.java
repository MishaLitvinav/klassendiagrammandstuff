package GameManagement.UserManagement.User;

public interface IUser {
    public void setUserName(String userName);
    public void setPassword(String password);
    public String getUserName();
    public String getPassword();   //passwort änderung möglich machen
}
