package Network;

import Message.IMessage;
import com.google.common.eventbus.EventBus;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;


public class ServerHandler extends ChannelInboundHandlerAdapter {

    private EventBus eb;

    public ServerHandler(EventBus eventBus) {
        this.eb = eventBus;
    }


    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg)
    {
        //PRUEFEN AUF IMESSAGE

        //TODO: Observerpattern
        if(msg instanceof IMessage){
            IMessage iMsg = (IMessage)msg;
            iMsg.setCtx(ctx);
            this.eb.post(msg);
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx)
    {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        ctx.close();
    }
}
