package Network;

import Events.CommandListener;
import Events.MessageHandler;
import Events.MessageListener;
import Message.IMessage;
import com.google.common.eventbus.EventBus;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import user.Commands.LoginCommand;
import user.Commands.RegisterCommand;


public class Server
{

    private int PORT;
    private EventBus eventBus;
    private MessageHandler handler;

    public Server(int port)
    {
        this.eventBus = new EventBus();
        this.handler = new MessageHandler(eventBus);
        this.PORT = port;
        this.registerListener();


        LoginCommand loginCommand = new LoginCommand("test", "1234");
        eventBus.post(loginCommand);
        eventBus.post(loginCommand);
    }


    public void start() throws Exception {

        // Configure the server.
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .option(ChannelOption.SO_BACKLOG, 100)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            ChannelPipeline p = ch.pipeline();
                            ch.pipeline().addLast(new ObjectEncoder());
                            ch.pipeline().addLast(new ObjectDecoder(ClassResolvers.cacheDisabled(null)));
                            ch.pipeline().addLast(new ServerHandler(getEventBus()));
                        }
                    });

            // Start the server.
            ChannelFuture future = serverBootstrap.bind(PORT).sync();
            // Wait until the server socket is closed.
            future.channel().closeFuture().sync();
        } finally {
            // Shut down all event loops to terminate all threads.
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    public void registerListener()
    {
        try {
            CommandListener listener = new CommandListener(this.handler);
            MessageListener messageListener = new MessageListener();
            this.eventBus.register(listener);
            this.eventBus.register(messageListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     *
     * @return this.eventBus
     */
    public EventBus getEventBus()
    {
        return this.eventBus;
    }


    /**
     * Eingehende Messages werden von hier auf den Eventbus geschrieben.
     *
     * @param in
     */
    public void receivedMessage(IMessage in)
    {
        this.eventBus.post(in);
    }
}
