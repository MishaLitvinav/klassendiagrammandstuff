package Validator;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Validation {

    /**
     * Überprüft ob wirklich das ankam, was der Client senden sollte.
     * Die Prüfkriterien sollten mit Client übereinstimmen.
     */
    public boolean validateName(String input){

        return input.length() > 0 && input.length() <= 30;
        //TODO: auf semikolons Prüfen
    }

    public boolean validatePassword(String input){
        return input.length() > 0 && input.length() <= 60;
    }

    public boolean validateEmail(String input){
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$");
        Matcher matcher = pattern.matcher(input);
        return matcher.matches() && input.length() <= 60;
    }

    /**
     * Normalisiert den Namen für die Datenbank .
     */
    public String properCaseName (String input) {
        //TODO: auf zahlen etc. muss geachtet werden -> Regeln für username -> Commonobjekte erweitern wie 'NichtErlaubterNutzernameMessage'
        return input.toLowerCase();
    }
}
