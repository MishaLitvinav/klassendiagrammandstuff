import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.util.Properties;

/**
 *
 * Klasse um moegliche Konfigurationen im Server durchzufuehren.
 */

public  class ConfigHandler {

    static Properties clientProps;


    public ConfigHandler()
    {
        getConfigFile();
    }

    private static void getConfigFile()
    {
        File cfg = new File(System.getProperty("user.dir") + "/config/" + "client.cfg");
        if(!cfg.exists())
        {
            //TODO: Hier muss man dann nochmal gucken was man schlaues machen kann.
        }
        else
        {
            try
            {
                clientProps.load(new FileInputStream(cfg));
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    private static void setClientProps()
    {

    }

}
