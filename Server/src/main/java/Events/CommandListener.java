package Events;

import GameManagement.UserManagement.IUserManagentImpl;
import GameManagement.UserManagement.User.IUserImpl;
import Message.Message;
import com.google.common.eventbus.Subscribe;
import io.netty.channel.ChannelHandlerContext;
import user.Commands.LoginCommand;
import user.Commands.LogoutCommand;
import user.Commands.RegisterCommand;
import user.Messages.LoginFailureMessage;
import user.Messages.LoginSuccessMessage;
import user.Messages.RegisterFailureMessage;
import user.Messages.RegisterSuccessMessage;

import java.sql.SQLException;

public class CommandListener {
    private IUserManagentImpl userManagent;
    private MessageHandler handler;

    public CommandListener(MessageHandler handler) throws Exception{
        this.handler = handler;
        this.userManagent = new IUserManagentImpl();
    }

    /**
     *
     * @param login
     * @return returns true if Login was successfull
     * @throws SQLException
     */
    @Subscribe
    public void checkLoginCommand(LoginCommand login) throws SQLException {
        if(login instanceof Message){
            boolean correctLogin = false;
            boolean userIsLoggedIn = false;
            ChannelHandlerContext ctx = (ChannelHandlerContext) login.getCtx();

            //überprüft, ob LoginDaten in DB existieren
            correctLogin = userManagent.logMeIn(login.getUsername(), login.getPassword());

            if(correctLogin){
                //case1: User ist eingelogt
                for(IUserImpl user : userManagent.getLoggedInUsers()){
                    if(login.getUsername() == user.getUserName()){
                        //UserAlreadyLoggedInMessage zurückgeben -> neäctser Sprint
                        userIsLoggedIn = true;
                        break;
                    }
                }
                if(!userIsLoggedIn){
                    IUserImpl user = new IUserImpl(login.getUsername(), login.getPassword());  //password ist nicht gehasht
                    userManagent.addUser(user);
                    LoginSuccessMessage successMessage = new LoginSuccessMessage();
                    successMessage.setCtx(ctx);
                    handler.sendMessage(successMessage);
                }
            } else {
                LoginFailureMessage failureMessage = new LoginFailureMessage();
                failureMessage.setCtx(ctx);
                handler.sendMessage(failureMessage);
            }
        }
    }

    @Subscribe
    public void checkRegistration(RegisterCommand register)throws SQLException{
        if(register instanceof Message){
            ChannelHandlerContext ctx = (ChannelHandlerContext) register.getCtx();
            boolean correctRegistration = false;
            correctRegistration = userManagent.registerMe(register.getUsername(), register.getPassword(), register.getEmail());
            if(correctRegistration){
                RegisterSuccessMessage rsm = new RegisterSuccessMessage();
                rsm.setCtx(ctx);
                handler.sendMessage(rsm);
            } else {
                RegisterFailureMessage rfm = new RegisterFailureMessage();
                rfm.setCtx(ctx);
                handler.sendMessage(rfm);
                System.out.println("Account already exists");
            }
        }
    }

    @Subscribe
    public void checkLogout(LogoutCommand logout) throws SQLException{
        if(logout instanceof Message){
            ChannelHandlerContext ctx = (ChannelHandlerContext) logout.getCtx();
            /*
            TODO 2. MEILENSTEIN:
            Check, if user is logedIn before sending logoutSuccessfullmessage. Need to check the session for that.
             */
        }
    }

    //Never used
    // private void sendToClient(ChannelHandlerContext ctx, IMessage msg){ ctx.writeAndFlush(msg); }
}
