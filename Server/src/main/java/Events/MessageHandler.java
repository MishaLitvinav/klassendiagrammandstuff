package Events;

import Message.Message;
import com.google.common.eventbus.EventBus;

public class MessageHandler{
    private EventBus bus;
    public MessageHandler(EventBus bus) {
        this.bus = bus;
    }

    public void sendMessage(Message msg){
        bus.post(msg);
    }
}
