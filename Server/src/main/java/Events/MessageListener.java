package Events;


import com.google.common.eventbus.Subscribe;
import io.netty.channel.ChannelHandlerContext;
import Message.Message;

/**
 * Hoert auf die Messages die auf dem Eventbus landen.
 */
public class MessageListener {

    @Subscribe
    public void messageToNettyListener(Message msg)
    {
        ChannelHandlerContext ctx = msg.getCtx();
        //TODO: PRUEFEN OB DAS AUCH ANDERS GEHT
        msg.setCtx(null);
        ctx.writeAndFlush(msg);
    }
}
